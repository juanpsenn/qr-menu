import Home from "./components/Home";
import Menu from "./components/Menu";

export const routes = [
    {
        exact: true,
        path: '/',
        component: Home
    },
    {
        path: '/menu',
        component: Menu
    }
]