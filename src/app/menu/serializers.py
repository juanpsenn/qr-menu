from rest_framework.serializers import ModelSerializer
from app.models import Menu, MenuSection, MenuItem


class MenuSerializer(ModelSerializer):

    class Meta:
        model = Menu
        depth = 1
        fields = '__all__'


class MenuItemSerializer(ModelSerializer):

    class Meta:
        model = MenuItem
        depth = 1
        exclude = ['is_visible']


class MenuSectionSerializer(ModelSerializer):
    items = MenuItemSerializer(many=True)

    class Meta:
        model = MenuSection
        depth = 1
        exclude = ['is_visible']
