from rest_framework.routers import SimpleRouter
from django.urls import path
from app.menu import views


router = SimpleRouter()

router.register(
    'menu',
    views.MenuViewSet
)
router.register(
    'section',
    views.MenuSectionViewSet
)
router.register(
    'item',
    views.MenuItemViewSet
)

urlpatterns = [
    path('show-menu/<int:menu_id>/', views.MenuSectionList.as_view()),
] + router.urls
