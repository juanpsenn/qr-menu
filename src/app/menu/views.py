from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from app.menu.serializers import MenuSerializer, MenuSectionSerializer, MenuItemSerializer
from app.models import Menu, MenuSection, MenuItem
from app.menu.logic_layer.selectors import sections_list

class MenuViewSet(ModelViewSet):
    queryset = Menu.objects.order_by('pk')
    serializer_class = MenuSerializer


class MenuSectionViewSet(ModelViewSet):
    queryset = MenuSection.objects.exclude(is_visible=False).order_by('pk')
    serializer_class = MenuSectionSerializer


class MenuItemViewSet(ModelViewSet):
    queryset = MenuItem.objects.exclude(is_visible=False).order_by('pk')
    serializer_class = MenuItemSerializer

class MenuSectionList(APIView):
    def get(self, request, menu_id):
        sections = sections_list(menu=menu_id)
        return Response(MenuSectionSerializer(sections, many=True).data, status=200)