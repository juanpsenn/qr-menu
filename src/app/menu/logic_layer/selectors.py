from app.models import MenuSection

def sections_list(*,
                  menu: int):
    return MenuSection.objects.prefetch_related('items').select_related('menu').filter(menu_id=menu)

