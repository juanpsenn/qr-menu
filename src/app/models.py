from django.db import models

# Create your models here.


class Menu(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.name}'


class MenuSection(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(blank=True,
                                   null=True)
    is_visible = models.BooleanField(default=True)
    menu = models.ForeignKey(
        'Menu',
        on_delete=models.SET_NULL,
        null=True,
        related_name='sections'
    )

    def __str__(self):
        return f'{self.name}'


class MenuItem(models.Model):
    name = models.CharField(
        max_length=128
    )
    description = models.TextField(
        blank=True,
        null=True
    )
    price = models.DecimalField(
        decimal_places=2,
        max_digits=15
    )
    is_visible = models.BooleanField(
        default=True
    )
    section = models.ForeignKey(
        'MenuSection',
        on_delete=models.SET_NULL,
        null=True,
        related_name='items'
    )

    def __str__(self):
        return f'{self.name}'
